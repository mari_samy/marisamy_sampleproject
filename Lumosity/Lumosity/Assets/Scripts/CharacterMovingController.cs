﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovingController : MonoBehaviour
{
    // initialize the variable for speed
    public float speed;
    // initialize the game objects (arrows)
    public List<GameObject> GameObjects;
    // initialize the boolean variable for downclick
    private bool downclick = false;
    // initialize the boolean variable for upclick
    private bool upclick = false;
    // initialize the variable for game object moving in x-direction
    private float XDirection;
    // initialize the variable for game object moving in y-direction
    private float YDirection;

    // Update is called once per frame
    public void Update()
    {
        if (downclick == true)
        {
            Debug.Log("downclick");
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }
        if (upclick == true)
        {
            Debug.Log("upclick");
            transform.Translate(0, speed * Time.deltaTime, 0);
        }

    }
    /// <summary>
    /// Click Down Arrow Button
    /// </summary>
    public void DownArrowButtonClicked()
    {
        Debug.Log("DownArrowButtonClicked");
        /// <summary>
        /// Down arrow should be move in down direction.
        /// </summary>
        if (transform.position.x > 0 || transform.position.y > 270)
        {
            Debug.Log("GameObjects[0]");
            downclick = true;
            Vector3 position = this.transform.position;
            XDirection = 0;
            YDirection = -3f;
            this.transform.position = position;
            if (Time.timeScale > 0)
            {
                Debug.Log("GameObjects[0], Time.timeScale > 0");
                transform.position = new Vector3(transform.position.x - (XDirection * speed), transform.position.y + (YDirection * speed), 0);
            }
        }
        /// <summary>
        /// Up  arrow should be move in up direction.
        /// </summary>
        if (transform.position.x > 70 || transform.position.y > 280)
        {
            Debug.Log("GameObjects[1]");
            upclick = true;
            Vector3 positions = this.transform.position;
            XDirection = 0;
            YDirection = 10f;
            this.transform.position = positions;
            if (Time.timeScale > 0)
            {
                Debug.Log("GameObjects[1] Time.timeScale > 0");
                transform.position = new Vector3(transform.position.x - (XDirection * speed), transform.position.y + (YDirection * speed), 0);
            }
        }
    }
}
